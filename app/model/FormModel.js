// Define the data structure for the form
Ext.define('MyApp.model.FormModel', {
    extend: 'Ext.data.Model',
    fields: ['inputText']
});
