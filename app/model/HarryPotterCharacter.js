Ext.define('MyApp.model.HarryPotterCharacter', {
    extend: 'Ext.data.Model',

    fields: [
        'id', 'character', 'nickname', 'hogwartsStudent', 'hogwartsHouse', 'interpretedBy', 'child', 'image'
    ]
});
