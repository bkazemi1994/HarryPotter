// app/model/ToDoModel.js

Ext.define('MyApp.model.ToDoModel', {
    extend: 'Ext.data.Model',

    fields: ['task']
});
